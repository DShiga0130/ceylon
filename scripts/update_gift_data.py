import csv
import os
import re

from django.db import transaction
from datetime import datetime as dt

from ceylontime.models import Guest, Gift
from ceylon_pj import settings

import traceback


def run(*args):
    # Create gift instance
    print('ギフトの種類を数値で入力してください')
    gift_type = input()
    print('何を送ったか分かる場合は品名を入力してください')
    gift = input()
    print('送付年月を入力してください(YYYY-MM-01)')
    send_date = input()

    if gift_type is '':
        gift_type = 0
    if send_date is '':
        send_date = None

    # 重複チェック
    registered_gift = Gift.objects.filter(gift_type=gift_type, gift=gift, send_date=send_date)
    if registered_gift:
        print('このギフトインスタンスは作成済みです')
        this_time_gift = registered_gift.first()

    else:
        this_time_gift = Gift(
            gift_type=gift_type, gift=gift, send_bool=True, send_date=send_date)

        this_time_gift.save()

    print('今回追加ギフト:' + this_time_gift.gift)

    # csv読み込み
    print('読み込むファイル名を入力してください')
    file_name = input()
    gift_data = os.path.join("ceylontime/gift_data/", file_name)
    gift_data_path = os.path.join(settings.BASE_DIR, gift_data)
    with open(gift_data_path, 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        # Gift add to Guest field
        for i, row in enumerate(reader):
            if i != 0:
                # 名前と郵便番号を取得
                full_name = row[5]
                postal_code = row[6]
                # 名前をlast_nameとfirst_nameに分けてGuestをfilter
                full_name_split = full_name.split()
                last_name = full_name_split[0]
                first_name = full_name_split[1]

                filter_by_guest_name = Guest.objects.filter(last_name=last_name, first_name=first_name)
                if filter_by_guest_name.first() is None:
                    filter_by_guest_name = Guest.objects.filter(first_name=full_name)
                print('======ただいまのGuest========')
                print(full_name)
                print(filter_by_guest_name)
                print('============================')
                if filter_by_guest_name:
                    guest = filter_by_guest_name

                    # 郵便番号チェック(同姓同名だった場合の絞り込み)
                    if postal_code:
                        guest = guest.filter(postal_code=postal_code)
                        # 一致するものがない、または複数だった時はnameで絞り込んだものを使用
                        if guest.count() == 1:
                            guest = guest.get(postal_code=postal_code)
                        else:
                            guest = filter_by_guest_name.first()
                    # 重複かつ郵便番号Noneで判定不可。
                    else:
                        if guest.count() > 1:
                            print('同姓同名かつ郵便番号がないため登録できません。')
                            pass

                    code = guest.code
                    last_name = guest.last_name
                    first_name = guest.first_name
                    postal_code = guest.postal_code

                    # 一致したGuestのgiftの重複チェック
                    registered_gift_relation = guest.gift.all()
                    if this_time_gift in registered_gift_relation:
                        print('既に会員番号' + code + 'に登録されているギフトです')
                        print('名前:' + last_name + first_name)
                        print('郵便番号:' + postal_code)
                    else:
                        guest.gift.add(this_time_gift)
                        print('会員番号' + code + 'のギフトを追加しました')
                        print('名前:' + last_name + first_name)
                        print('郵便番号:' + postal_code)

