
import csv
import os
from ceylon_pj import settings

from ceylontime.models import Guest

print('Input file name')
file_name = input()
import_data = os.path.join('ceylontime/import_data/', file_name)
import_data_path = os.path.join(settings.BASE_DIR, import_data)

with open(import_data_path, 'r', encoding='utf-8') as f:
    reader = csv.reader(f)
    header = next(reader)

    save_count = 0
    count = 0
    for row in reader:

        if not row[1]:
            continue

        row_code = row[1]
        row_cellphone = row[7] or ''
        row_mail = row[8] or ''
        if row_cellphone == '' and row_mail == '':
            continue

        code = '＃' + row_code[1:].zfill(7)
        guest = Guest.objects.filter(code=code).first()
        if not guest:
            print('Guest object not found: code-{}'.format(row_code))
            continue

        count += 1
        msg = ''
        if row_cellphone is not '':
            guest.cell_phone = row_cellphone
            guest.save()
            msg += 'cell phone: {}'.format(guest.cell_phone)

        if row_mail is not '':
            guest.mail = row_mail
            guest.save()
            msg += 'mail: {}'.format(guest.mail)

        msg = '{}：{} {} 更新\n    '.format(guest.code, guest.last_name, guest.first_name) + msg
        print(msg)

    print('---- ' + str(count) + '件 のGuest情報を更新しました----')

