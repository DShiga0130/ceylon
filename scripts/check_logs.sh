# Add this script to crontab:
# > crontab -e
# */5 * * * * SLACK_API_TOKEN=<token> VIRTUALENV_PATH=/path/to/virtualenv SCRIPT_FILE=/path/to/check_logs.py ERROR_LOG_FILE=/path/to/file bash /path/to/check_logs.sh >/dev/null 2>&1
#!/bin/sh
source ${VIRTUALENV_PATH}/bin/activate
python ${SCRIPT_FILE}
