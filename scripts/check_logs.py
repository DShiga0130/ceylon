import os
from shutil import copyfile
from slackclient import SlackClient

slack_token = os.environ['SLACK_API_TOKEN']
error_log_file = os.environ['ERROR_LOG_FILE']
sc = SlackClient(slack_token)

f = open(error_log_file, 'r')
current = f.read()
f.close()

f = open(error_log_file + '.last', 'r')
last = f.read()
f.close()

diff = current.replace(last, '')
if len(diff) > 0:
    sc.api_call('chat.postMessage', channel='ceylon_time', text=diff)

copyfile(error_log_file, error_log_file + '.last')
