#  python manage.py shell < ceylontime/import_data.py

import csv
import os
import re

from django.db import transaction
from datetime import datetime as dt

from ceylontime.models import Mediator, Guest, GuestFamily
from ceylon_pj import settings

import traceback


def get_mediator(code):

    head_num = code[0]
    if head_num == '1':
        mediator = Mediator.objects.get(mediator='ばんせい証券')
        return mediator

    if head_num == '2':
        mediator = Mediator.objects.get(mediator='石かわ')
        return mediator

    if head_num == '3':
        mediator = Mediator.objects.get(mediator='サティスファクトリー')
        return mediator

    if head_num == '5':
        mediator = Mediator.objects.get(mediator='ばんせいホールディングス')
        return mediator

    if head_num == '6':
        mediator = Mediator.objects.get(mediator='BIC')
        return mediator

    if head_num == '7':
        mediator = Mediator.objects.get(mediator='SPA CEYLON')
        return mediator

    if head_num == '8':
        mediator = Mediator.objects.get(mediator='店頭申込')
        return mediator

    raise ValueError

def run():

    print('Input file name')
    file_name = input()
    import_data = os.path.join("ceylontime/import_data/", file_name)
    import_data_path = os.path.join(settings.BASE_DIR, import_data)
    print(import_data_path)
    print('Input registered date')
    registration_date = input()

    try:
        with open(import_data_path, 'r', encoding='utf-8') as f:
            reader = csv.reader(f)
            header = next(reader)

            save_count = 0
            count = 0
            date_re = re.compile('[0-9]{4}/[0-9]+/[0-9]+')
            code_re = re.compile("＃([0-9]{6})")
            for row in reader:
                if not row[0]:
                    continue

                if not row[3]:
                    continue

                count += 1

                guest = Guest()
                row_code = row[1]

                m_code = code_re.match(row_code)

                if not m_code:
                    print('Error: ', row_code)
                    continue

                int_code = int(m_code[1])
                code = '＃' + '{:0=7}'.format(int_code)

                print(code)

                if Guest.objects.filter(code=code):
                    count -= 1
                    print('Duplicate guest.')
                    continue

                code_list_data = "ceylontime/members_code/access_token__code_list_20200713_update.csv"
                code_list_data_path = os.path.join(settings.BASE_DIR, code_list_data)
                with open(code_list_data_path, 'r', encoding='utf-8') as g:
                    g_reader = csv.DictReader(g)
                    g_header = next(g_reader)

                    r_count = 0
                    access_token = ''
                    for r in g_reader:
                        r_count += 1

                        if str(int_code) == r['code']:
                            access_token = r['access_token']
                            break
                        else:
                            continue

                guest.code = code
                if access_token == '':
                    access_token = str(int_code)
                    if re.match('＃5[0-9]{5}', code):
                        print('会員番号' + code + '番の登録ができませんでした：対応表でaccess_tokenが見つけられませんでした。')
                        continue

                guest.access_token = access_token
                print('code: ' + code + 'access_token:' + access_token)

                name_re = re.compile("(.*)\s+(.*)")
                name_k = name_re.match(row[2])
                if name_k:
                    guest.first_name_kana = name_k[2]
                    guest.last_name_kana = name_k[1]
                else:
                    print('会員番号' + code + '番の登録ができませんでした：カナ表記が正しくありません。')
                    continue

                name = name_re.match(row[3])
                if name:
                    guest.first_name = name[2]
                    guest.last_name = name[1]
                else:
                    print('会員番号' + code + '番の登録ができませんでした：顧客名の表記が正しくありません。')
                    continue

                date = row[4]
                m_date = date_re.match(date)
                if m_date:
                    guest.birthday = dt.strptime(date, '%Y/%m/%d')

                if row[5] == '男性' or row[5] == '男':
                    guest.gender = Guest.MALE
                elif row[5] == '女性' or row[5] == '女':
                    guest.gender = Guest.FEMALE
                else:
                    guest.gender = 0

                guest.phone = ''
                if row[6] is not None:
                    guest.phone = row[6]

                guest.cell_phone = ''
                if row[7] is not None:
                    guest.cell_phone = row[7]

                guest.mail = ''
                if row[8] is not None:
                    guest.mail = row[8]

                guest.postal_code = ''
                if row[9] is not None:
                    guest.postal_code = row[9]

                guest.address1 = ''
                if row[10] is not None:
                    guest.address1 = row[10]

                guest.job = ''
                if row[11] is not None:
                    guest.job = row[11]

                guest.company = ''
                if row[12] is not None:
                    guest.company = row[12]

                guest.company_department = ''
                if row[13] is not None:
                    guest.company_department = row[13]

                guest.company_roll = ''
                if row[14] is not None:
                    guest.company_roll = row[14]

                guest.company_address = ''
                if row[15] is not None:
                    guest.company_address = row[15]

                guest.company_tel = ''
                if row[16] is not None:
                    guest.company_tel = row[16]

                mediator = get_mediator(str(int_code))
                guest.customer_of = mediator

                if registration_date:
                    guest.registration_date = registration_date

                guest.save()

                save_count += 1
                print('----- ' + str(save_count) + ' code: ' + code + 'access_token:' + access_token + ' -----')

                if row[17]:
                    gf = GuestFamily()
                    gf.guest = guest
                    gf.name = row[17]
                    date = row[18]

                    m_date = date_re.match(date)
                    if m_date:
                        gf.birthday = dt.strptime(date, '%Y/%m/%d')
                    gf.save()

                if row[19]:
                    gf = GuestFamily()
                    gf.guest = guest
                    gf.name = row[19]
                    date = row[20]
                    m_date = date_re.match(date)
                    if m_date:
                        gf.birthday = dt.strptime(date, '%Y/%m/%d')
                    gf.save()

                if row[21]:
                    gf = GuestFamily()
                    gf.guest = guest
                    gf.name = row[21]
                    date = row[22]
                    m_date = date_re.match(date)
                    if m_date:
                        gf.birthday = dt.strptime(date, '%Y/%m/%d')
                    gf.save()

            print('---- ' + str(save_count) + '件 のインスタンスが生成されました----')
    except:
        print(traceback.format_exc())
