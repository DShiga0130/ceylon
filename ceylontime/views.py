from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, AccessMixin
from django.contrib.auth.models import User, Group
from django.core import mail
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.db.models import Value, Q, Count
from django.db.models.functions import Concat
from django.http import JsonResponse, Http404
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.views.decorators.http import require_http_methods

from .templatetags import generate_history_row
from .models import Mediator, Guest, VisitHistory, Shop, Role, DMHistory, DMTemplate
from .forms import DMTemplateCreateForm

import json
import re

import logging
logger = logging.getLogger('django')

class AjaxableResponseMixin:
    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
                'success_url': self.success_url
            }
            return JsonResponse(data)
        else:
            return response


# TODO create and CreateView, UpdateView, DetailView and DeleteView for User
class UserCreateView(LoginRequiredMixin, generic.edit.CreateView):
    model = User
    second_model = Role
    template_name = 'ceylontime/user_create.html'
    fields = ['username', 'groups', 'first_name', 'last_name', 'email', 'role']
    success_url = reverse_lazy('ceylontime:home')


class UserDetailView(LoginRequiredMixin, generic.DetailView):
    model = User
    template_name = 'ceylontime/user_detail.html'

    # def get_context_data(self, **kwargs):
    #     context = super(UserDetailView, self).get_context_data(**kwargs)
    #     context['role'] = Role.objects.get(users=self.object)
    #     context['group'] = Group.objects.get(user=self.object)
    #     return context

    # def get_object(self, queryset=None):
    #     role = super(UserDetailView, self).get_object().role_set.get()
    #     group = super(UserDetailView, self).get_object().groups.get()
    #     return role


class UserListView(LoginRequiredMixin, generic.ListView):
    model = User
    template_name = 'ceylontime/user_list.html'
    paginate_by = 15

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        context['role'] = Role.objects.all()
        return context


class UserUpdateView(LoginRequiredMixin, generic.edit.UpdateView):
    model = User
    template_name = 'ceylontime/user_update.html'
    fields = ['username', 'groups', 'first_name', 'last_name', 'email']

    def get_success_url(self):
        return reverse('ceylontime:user-detail', args=[self.id])


class UserDeleteView(LoginRequiredMixin, generic.edit.DeleteView):
    model = User
    template_name = 'ceylontime/user_delete.html'
    success_url = reverse_lazy('ceylontime:home')


class ShopDetailView(LoginRequiredMixin, generic.DetailView):
    model = Shop
    template_name = 'ceylontime/shop_detail.html'

    def get_object(self):
        obj = super().get_object()
        if self.request.user.has_perm('view_shop', obj):
            return obj
        else:
            raise Http404()


class GuestListView(LoginRequiredMixin, generic.ListView):
    model = Guest
    template_name = 'ceylontime/guest_list.html'
    paginate_by = 100
    # TODO add columns of reserviation and remote ordered??


class DuringServeToGuestListViews(LoginRequiredMixin, generic.ListView):
    model = VisitHistory
    template_name = 'ceylontime/home.html'

    def get_queryset(self):
        if self.request.user.groups.filter(name='executive').count() > 0:
            return VisitHistory.objects.filter(visit_status=VisitHistory.IN_VISIT).order_by('-visit_date')
        else:
            return VisitHistory.objects.filter(
                visit_status=VisitHistory.IN_VISIT,
                in_shop_id__in=[g.id for g in self.request.user.groups.all()]
            ).order_by('-visit_date')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tab_index'] = 0
        return context


class GuestDetailView(LoginRequiredMixin, generic.DetailView):
    model = Guest
    template_name = 'ceylontime/guest_detail.html'


# TODO: Implement permission
class UserUpdateGuestView(LoginRequiredMixin, generic.edit.UpdateView):
    model = Guest
    fields = (
        'birthday',
        'phone',
        'cell_phone',
        'postal_code',
        'address1',
        'address2',
        'mail',
        'job',
        'company',
        'company_department',
        'company_roll',
        'company_address',
        'company_tel',
    )
    template_name = 'ceylontime/user_update_guest.html'
    permission_required = ('ceylontime.change_guest')
    raise_exception = True

    def handle_no_permission(self):
        raise Http404


class MediatorDetailView(LoginRequiredMixin, PermissionRequiredMixin, AccessMixin, generic.DetailView):
    model = Mediator
    template_name = 'ceylontime/mediator_detail.html'
    permission_required = ('ceylontime.view_mediator')
    raise_exception = True

    def handle_no_permission(self):
        raise Http404


class VisitHistoryListView(LoginRequiredMixin, generic.ListView):
    model = VisitHistory
    template_name = 'ceylonitme/visithistory_list.html'

    def render_to_response(self, context, **kwargs):
        if self.request.is_ajax():
            return JsonResponse(self.get_ajax_json_data(context), **kwargs)
        else:
            return super(VisitHistoryListView, self).render_to_response(context, **kwargs)

    def get_ajax_json_data(self, context):
        result = []
        for h in self._get_histories():
            result.append(generate_history_row(h))
        return { 'tags': result }

    def get_queryset(self):
        if self.request.is_ajax():
            return None
        else:
            return self._get_histories()

    def _get_histories(self):
        if self.request.user.groups.filter(name='executive').count() > 0:
            history = VisitHistory.objects.all()
        else:
            history = VisitHistory.objects.filter(in_shop_id__in=[ g.id for g in self.request.user.groups.all() ])
        sort_order = self.request.GET.get('sort_order', 'register_date')
        desc = self.request.GET.get('desc', 'true').lower() == 'true'
        if sort_order == 'shop':
            history = history.order_by('{}in_shop__shop_group__name'.format('-' if desc else ''))
        elif sort_order == 'name':
            history = history.annotate(name=Concat('guest_name__last_name', Value(' '), 'guest_name__first_name')).order_by('{}name'.format('-' if desc else ''))
        else:
            history = history.order_by('{}{}'.format('-' if desc else '', sort_order))
        return history

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tab_index'] = 1
        return context


class VisitDetailView(LoginRequiredMixin, generic.DetailView):
    model = VisitHistory
    template_name = 'ceylontime/visit_detail.html'


class CreateVisitHistoryView(AjaxableResponseMixin, generic.edit.CreateView):
    model = VisitHistory
    template_name = 'ceylontime/new_visithistory.html'
    fields = ['guest_name', 'number_of_visitors', 'in_shop']
    success_url = reverse_lazy('ceylontime:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        guest = Guest.objects.filter(pk=self.request.GET.get('guest_pk')).first()
        if guest:
            context['form'].fields['guest_name'].initial = guest
        return context

    def form_valid(self, form):
        form.instance.visit_status = VisitHistory.IN_VISIT
        return super().form_valid(form)


class UpdateLeaveShopView(LoginRequiredMixin, generic.edit.UpdateView):
    model = VisitHistory
    fields = [
        'number_of_visitors',
        'payment',
        'service_pattern',
        'exist_reservation',
        'reservation_remark',
        'salesperson',
        'comment'
    ]
    template_name = 'ceylontime/update_leave_shop.html'
    success_url = reverse_lazy('ceylontime:home')

    def form_valid(self, form):
        payment = form.instance.payment
        if payment > 0:
            form.instance.visit_status = VisitHistory.NOT_VISIT
        # else payment = 0:
        # TODO set implement that display dialog caution 'Aren't you sure that the input amount is 0'
        # else payment = None:
        # TODO set implement that display dialog caution 'Please input the amount'

        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)


class UpdateVisitHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AccessMixin, generic.edit.UpdateView):
    model = VisitHistory
    fields = [
        'number_of_visitors',
        'payment',
        'service_pattern',
        'exist_reservation',
        'reservation_remark',
        'salesperson',
        'comment'
    ]
    template_name = 'ceylontime/update_visithistory.html'
    permission_required = ('ceylontime.change_visithistory')
    raise_exception = True

    def handle_no_permission(self):
        raise Http404


class DeleteVisitHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AccessMixin, generic.edit.DeleteView):
    model = VisitHistory
    template_name = 'ceylontime/delete_visithistory.html'
    success_url = reverse_lazy('ceylontime:home')
    permission_required = ('ceylontime.change_visithistory')
    raise_exception = True

    def handle_no_permission(self):
        raise Http404


@login_required
@require_http_methods(['GET'])
def search_guest(request):
    token = request.GET.get('token', None)
    if token:
        g = Guest.objects.filter(access_token=token).first()
        if g:
            return JsonResponse({
                'id': g.id,
                'first_name_kana': g.first_name_kana,
                'last_name_kana': g.last_name_kana,
                'first_name': g.first_name,
                'last_name': g.last_name,
                'family': [ f.name for f in g.family.all() ],
            })
        else:
            raise Http404()
    raise Http404()


def _get_guest_tag_for_search_box(guest):
    return """<li data-id="%(id)d" title="%(name)s">
    <a href="%(detail_page_link)s" target="_blank">
        <span class="name">%(name)s</span>
    </a>
</li>""" % {
    'id': guest.id,
    'name': '{} {} ({} {})'.format(
        guest.last_name,
        guest.first_name,
        guest.last_name_kana,
        guest.first_name_kana
    ),
    'detail_page_link': reverse(
        'ceylontime:guest-detail', kwargs={ 'pk': guest.id }
    )
}


@login_required
@require_http_methods(['GET'])
def search_guest_by_keyword(request):
    timestamp = request.GET.get('timestamp', 0);
    keyword = request.GET.get('keyword', None)
    print(keyword)
    if timestamp and keyword:
        items = ''
        for g in Guest.objects.filter(\
            Q(first_name__contains=keyword) | \
            Q(last_name__contains=keyword) | \
            Q(first_name_kana__contains=keyword) | \
            Q(last_name_kana__contains=keyword)
        ):
            items += _get_guest_tag_for_search_box(g)
        return JsonResponse({
            'timestamp': timestamp,
            'items': items
        })
    else:
        return JsonResponse({}, status=400)


@login_required
def send_test_mail(request, template):
    name = request.user.username
    subject = template.subject
    body = template.body

    subject_for_user = re.sub(r'\$氏名\$', str(name), subject, re.UNICODE | re.MULTILINE)
    body_for_user = re.sub(r'\$氏名\$', str(name), body, re.UNICODE | re.MULTILINE)
    print(subject_for_user)
    print(body_for_user)

    sender = settings.EMAIL_FROM
    address = template.test_mail

    email = create_html_mail(
        subject=subject_for_user,
        body=body_for_user,
        from_email=sender,
        to=[address],
    )
    email.send()


def create_html_mail(subject, body, from_email, to):

    email = EmailMultiAlternatives(
        subject=subject,
        body=body,
        from_email=from_email,
        to=to,
    )
    email.attach_alternative(body, "text/html")
    return email


class DMTemplateListView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    template_name = 'ceylontime/dm/template/list.html'
    model = DMTemplate
    context_object_name = 'templates'
    permission_required = ("ceylontime.view_dmtemplate",)
    raise_exception = True
    ordering = ['-created_at']


class DMTemplateCreateView(LoginRequiredMixin, PermissionRequiredMixin, generic.edit.CreateView):
    template_name = 'ceylontime/dm/template/create_form.html'
    model = DMTemplate
    context_object_name = 'template'
    permission_required = ("ceylontime.add_dmtemplate",)
    raise_exception = True
    #fields = ['name', 'subject', 'body', 'test_mail']
    form_class = DMTemplateCreateForm
    email = []

    def form_valid(self, form):
        template = form.save(commit=False)
        template.created_by = self.request.user
        template.updated_by = self.request.user

        send_test_mail(self.request, template)

        messages.add_message(self.request, messages.INFO, 'DMテンプレート「%s」を作成し、テストメールを送付しました' % template.name)

        return super().form_valid(form)


class DMTemplateUpdateView(LoginRequiredMixin, PermissionRequiredMixin, generic.edit.UpdateView):
    template_name = 'ceylontime/dm/template/update_form.html'
    model = DMTemplate
    context_object_name = 'template'
    permission_required = ("ceylontime.change_dmtemplate",)
    raise_exception = True
    form_class = DMTemplateCreateForm
    #fields = ['name', 'subject', 'body', 'test_mail']

    def form_valid(self, form):
        template = form.save(commit=False)
        template.updated_by = self.request.user

        send_test_mail(self.request, template)

        messages.add_message(self.request, messages.INFO, 'DMテンプレート「%s」を更新し、テストメールを送付しました' % template.name)

        return super().form_valid(form)


class DMTemplateDeleteView(LoginRequiredMixin, PermissionRequiredMixin, generic.edit.DeleteView):
    template_name = 'ceylontime/dm/template/delete_form.html'
    model = DMTemplate
    context_object_name = 'template'
    permission_required = ("ceylontime.delete_dmtemplate",)
    raise_exception = True

    def get_object(self):
        object = super().get_object()
        if object:
            self.dm_template_name = object.name
        return object

    def get_success_url(self):
        messages.add_message(self.request, messages.INFO, 'DMテンプレート「%s」を削除しました' % self.dm_template_name)
        return reverse('ceylontime:dm-template-list')

class DMTemplateDetailView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    template_name = 'ceylontime/dm/template/detail.html'
    model = DMTemplate
    context_object_name = 'template'
    permission_required = ("ceylontime.view_dmtemplate",)
    raise_exception = True

class DMListView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    template_name = 'ceylontime/dm/list.html'
    model = DMHistory
    context_object_name = 'dms'
    ordering = ['-datetime']
    permission_required = ("ceylontime.view_dmhistory",)
    raise_exception = True

class DMSetupView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    template_name = 'ceylontime/dm/setup.html'
    model = Guest
    context_object_name = 'candidates'
    permission_required = ("ceylontime.add_dmhistory",)
    raise_exception = True

    def get_queryset(self):
        conditions = {}
        visit_count = self.request.GET.get('visit_count', None)
        if visit_count and visit_count.isdigit():
            conditions['visithistory_count__gte'] = int(visit_count)
        mediator = self.request.GET.get('mediator', None)
        if mediator and mediator.isdigit():
            conditions['customer_of__id'] = int(mediator)
        elif mediator and mediator == 'none':
            conditions['customer_of'] = None
        candidates = Guest.objects \
            .exclude(mail=None) \
            .exclude(mail='') \
            .annotate(visithistory_count=Count('visithistory')).filter(**conditions)
        self.candidates_count = candidates.count()
        return candidates

    def _get_selected_candidates(self):
        candidates = []
        if self.request.method == 'GET':
            for id in self.request.GET.get('candidates', '').split(','):
                if id.isdigit():
                    candidates.append(int(id))
        else:
            for key, value in self.request.POST.items():
                if key.isdigit() and value == 'on':
                    candidates.append(int(key))
        return candidates

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if 'template_id' in request.session:
            del request.session['template_id']
        if 'mail_subject' in request.session:
            del request.session['mail_subject']
        if 'mail_body' in request.session:
            del request.session['mail_body']
        return response

    def post(self, request, *args, **kwargs):
        candidates = self._get_selected_candidates()
        if len(candidates) > 0:
            url = reverse('ceylontime:dm-send')
            parameters = [ 'receivers={}'.format(','.join([ str(c) for c in candidates ])) ]
            visit_count = self.request.POST.get('visit_count', None)
            if visit_count and visit_count.isdigit():
                parameters.append('visit_count={}'.format(visit_count))
            mediator = self.request.POST.get('mediator', None)
            if mediator:
                parameters.append('mediator={}'.format(mediator))
            return redirect('{}?{}'.format(url, '&'.join(parameters)))
        else:
            messages.add_message(request, messages.ERROR, '宛先を選択してください', extra_tags='close-by-button')
            return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        candidates = self._get_selected_candidates()
        context['selected_candidates'] = candidates
        context['all_selected'] = len(candidates) == self.candidates_count
        context['mediators'] = Mediator.objects.all()
        return context


class DMSendView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    template_name = 'ceylontime/dm/send.html'
    model = Guest
    context_object_name = 'receivers'
    permission_required = ("ceylontime.add_dmhistory",)
    raise_exception = True

    def get_queryset(self):
        receivers = self.request.GET.get('receivers', '').split(',')
        ids = []
        for r in receivers:
            if r.isdigit():
                ids.append(int(r))
        return Guest.objects.filter(id__in=ids)

    def post(self, request, *args, **kwargs):
        template_id = request.POST.get('dm_template', None)
        template = get_object_or_404(DMTemplate, id=template_id)
        subject = template.subject
        body = template.body
        sender = settings.EMAIL_FROM
        emails = []
        receivers = []

        try:
            for r in request.POST.get('receivers', '').split(','):
                if r.isdigit():
                    guest = Guest.objects.filter(id=int(r)).first()
                    if guest and guest.mail:
                        subject_for_guest = re.sub(r'\$氏名\$', str(guest), subject, re.UNICODE | re.MULTILINE)
                        body_for_guest = re.sub(r'\$氏名\$', str(guest), body, re.UNICODE | re.MULTILINE)
                        receivers.append(guest)
                        emails.append(create_html_mail(
                            subject=subject_for_guest,
                            body=body_for_guest,
                            from_email=sender,
                            to=[guest.mail],
                        ))
                        if len(emails) >= 100:
                            connection = mail.get_connection()
                            connection.send_messages(emails)
                            connection.close()
                            emails = []

            history = DMHistory.objects.create(
                sender=sender,
                subject=subject,
                body=body,
                sent_by=request.user,
            )
            history.receivers.add(*receivers)

        except Exception as e:
            error = str(e)
            logger.error('DMConfirmView.post: {}'.format(error))
            messages.add_message(self.request, messages.ERROR, 'DM送信に失敗しました。開発者にお問い合わせください', extra_tags='close-by-button')
            messages.add_message(self.request, messages.ERROR, 'エラー: {}'.format(error))
            url = reverse('ceylontime:dm-send')
            return redirect('{}?receivers={}'.format(
                url, request.POST.get('receivers', '')
            ))
        messages.add_message(self.request, messages.INFO, 'DM送信完了しました')
        return redirect(reverse('ceylontime:dm-list'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['template_id'] = self.request.session.get('template_id', '')
        context['subject'] = self.request.session.get('mail_subject', None)
        context['body'] = self.request.session.get('mail_body', None)
        context['sender'] = settings.EMAIL_FROM
        context['templates'] = DMTemplate.objects.order_by('-created_at')
        return context

class DMHistoryDetailView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    template_name = 'ceylontime/dm/detail.html'
    model = DMHistory
    context_object_name = 'dm'
    permission_required = ("ceylontime.view_dmhistory",)
    raise_exception = True
