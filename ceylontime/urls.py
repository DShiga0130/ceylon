from django.urls import path

from . import views

app_name = 'ceylontime'
urlpatterns = [
    path('', views.DuringServeToGuestListViews.as_view(), name='home'),
    path('new/user/', views.UserCreateView.as_view(), name='user-create'),
    path('user/<int:pk>/', views.UserDetailView.as_view(), name='user-detail'),
    path('user/', views.UserListView.as_view(), name='user-list'),
    path('user/<int:pk>/update', views.UserUpdateView.as_view(), name='user-update'),
    path('user/<int:pk>/delete', views.UserDeleteView.as_view(), name='user-delete'),
    path('shop/<int:pk>/', views.ShopDetailView.as_view(), name='shop-detail'),
    path('guests/search/', views.search_guest, name='guest-search'),
    path('guests/search-by-keyword/', views.search_guest_by_keyword, name='guest-keyword-search'),
    path('guest/', views.GuestListView.as_view(), name='guest-list'),
    path('guest/<int:pk>/', views.GuestDetailView.as_view(), name='guest-detail'),
    path('guest/<int:pk>/user-update', views.UserUpdateGuestView.as_view(), name='user-update-guest'),
    path('mediator/<int:pk>/', views.MediatorDetailView.as_view(), name='mediator-detail'),
    path('history/', views.VisitHistoryListView.as_view(), name='visithistory-list'),
    path('history/<int:pk>/', views.VisitDetailView.as_view(), name='visit-detail'),
    path('new/history/', views.CreateVisitHistoryView.as_view(), name='new-visit'),
    path('history/<int:pk>/update/', views.UpdateVisitHistoryView.as_view(), name='update-visit'),
    path('history/<int:pk>/leave_shop/', views.UpdateLeaveShopView.as_view(), name='leave-shop'),
    path('history/<int:pk>/delete/', views.DeleteVisitHistoryView.as_view(), name='delete-visit'),
    path('dm-templates/', views.DMTemplateListView.as_view(), name='dm-template-list'),
    path('dm-templates/new/', views.DMTemplateCreateView.as_view(), name='dm-template-new'),
    path('dm-template/<int:pk>/update/', views.DMTemplateUpdateView.as_view(), name='dm-template-update'),
    path('dm-template/<int:pk>/delete/', views.DMTemplateDeleteView.as_view(), name='dm-template-delete'),
    path('dm-template/<int:pk>/', views.DMTemplateDetailView.as_view(), name='dm-template-detail'),
    path('dms/', views.DMListView.as_view(), name='dm-list'),
    path('dm/<int:pk>/', views.DMHistoryDetailView.as_view(), name='dm-detail'),
    path('dm/setup/', views.DMSetupView.as_view(), name='dm-setup'),
    path('dm/send/', views.DMSendView.as_view(), name='dm-send'),
]

# TODO create various edit views of User __ Accounts(Users) are made by people other than themselves, who should be logged in.
# TODO create edit(change) view of user models
