from django import template
from django.urls import reverse
from django.utils import timezone
from django.utils.html import escape
from django.utils.safestring import mark_safe

import re

register = template.Library()


@register.filter
def message_level(messages):
    tags = set()
    for m in messages:
        if 'close-by-button' in m.tags:
            tags.add('close-by-button')
        if 'error' in m.tags:
            tags.add('error')
    return ' '.join(list(tags))

@register.filter
def paragraph(text):
    element = ''
    text = escape(text)
    for line in text.splitlines():
        line = re.sub('\s|　|\t', '&nbsp;', line)
        element += '<p>%(line)s</p>' % { 'line': line if len(line) > 0 else '&nbsp;' }
    return mark_safe(element)

@register.filter
def ellipsis(value, limit=80):
    try:
        limit = int(limit)
    except ValueError:
        return value

    if len(value) <= limit:
        return value

    value = value[:limit]
    return value + '...'

@register.filter(name='generate_history_row')
def generate_history_row(history):
    return """<tr>
    <td>%(in_shop)s</td>
    <td><a href="%(guest_url)s">%(last_name)s %(first_name)s</a></td>
    <td><a href="%(history_url)s">%(visit_date)s</a></td>
    <td class="number">%(number_of_visitors)d</td>　
    <td class="number">%(payment)s</td>
</tr>""" % {
    'in_shop': history.in_shop,
    'guest_url': reverse('ceylontime:guest-detail', kwargs={ 'pk': history.guest_name.pk }),
    'last_name': history.guest_name.last_name,
    'first_name': history.guest_name.first_name,
    'history_url': reverse('ceylontime:visit-detail', kwargs={ 'pk': history.pk }),
    'visit_date': timezone.localtime(history.visit_date).strftime('%Y/%m/%d %H:%M'),
    'number_of_visitors': history.number_of_visitors,
    'payment': '{:,}'.format(history.payment) if history.payment else 0,
}
