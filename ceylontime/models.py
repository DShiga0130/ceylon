import jaconv
from datetime import date, datetime
from django.db import models
from django.contrib.auth.models import User, Group
from django.urls import reverse


def to_zenkaku(string):

    if not string:
        return ''

    if not isinstance(string, str):
        return string

    return jaconv.h2z(string, digit=True, ascii=True)


def modify_guest_name():

    non_last_name_guests = Guest.objects.filter(last_name='')
    count = 0
    for guest in non_last_name_guests:
        name = guest.first_name
        if name:
            name = name.split()
        else:
            print("None first name guest: {}".format(guest))
            continue

        if len(name) <= 1:
            continue

        guest.last_name = name[0]
        guest.first_name = name[1]
        guest.save()
        print('{}{}'.format(guest.last_name, guest.first_name))
        count += 1

    print("Modified {} guest name".format(count))
    return


def modify_guest_kana():

    non_last_kana_guests = Guest.objects.filter(last_name_kana='')
    count = 0
    for non_last_kana_guest in non_last_kana_guests:
        kana = non_last_kana_guest.first_name_kana
        if kana:
            kana = kana.split()
        else:
            print("None first name kana guest: {}".format(non_last_kana_guest))
            continue

        if len(kana) <= 1:
            continue

        non_last_kana_guest.last_name_kana = kana[0]
        non_last_kana_guest.first_name_kana = kana[1]
        non_last_kana_guest.save()
        print('{} {}'.format(non_last_kana_guest.last_name_kana, non_last_kana_guest.first_name_kana))
        count += 1

    print("Modified {} guest kana".format(count))
    return


def modify_guest_kana_to_zenkaku():

    count = 0
    for g in Guest.objects.all():
        current_first_kana = g.first_name_kana
        current_last_kana = g.last_name_kana
        first_name_kana = to_zenkaku(g.first_name_kana)
        last_name_kana = to_zenkaku(g.last_name_kana)
        g.first_name_kana = first_name_kana
        g.last_name_kana = last_name_kana
        print("{} {} → {} {}".format(current_last_kana, current_first_kana, last_name_kana, first_name_kana))

        g.save()

        count += 1

    print("Modified {} guests kana convert to zenkaku".format(count))


class Shop(models.Model):
    shop_group = models.OneToOneField(Group, on_delete=models.DO_NOTHING, primary_key=True, verbose_name='店舗名（グループ名）,')
    phone = models.CharField('TEL', max_length=255)
    address1 = models.CharField('住所1', max_length=255)
    address2 = models.CharField('住所2', max_length=255)
    manager = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='shop_manager', null=True)

    class Meta:
        permissions = (
            # ("view_shop", "View shop"),
        )

    def __str__(self):
        return f'{self.shop_group}'


class Role(models.Model):
    name = models.CharField('役割', max_length=255)
    users = models.ManyToManyField(User)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='role_created_by')
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='role_updated_by')

    def __str__(self):
        return self.name


class Mediator(models.Model):
    mediator = models.CharField('紹介元', max_length=255)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='mediator_created_by')
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='mediator_updated_by')

    def __str__(self):
        return self.mediator

    class Meta:
        permissions = (
            # ("view_mediator", "View mediator"),
        )


class Guest(models.Model):

    NOT_CHOICE = 0
    MALE = 1
    FEMALE = 2
    GENDER_CHOICE = (
        (NOT_CHOICE, 'Not choice'),
        (MALE, '男'),
        (FEMALE, '女'),
    )

    # general information
    code = models.CharField('会員番号', max_length=20)
    first_name = models.CharField('名', max_length=20)
    first_name_kana = models.CharField('名（カナ）', max_length=40)
    last_name = models.CharField('氏', max_length=20)
    last_name_kana = models.CharField('氏（カナ）', max_length=40)
    birthday = models.DateField('生年月日', null=True, blank=True)
    gender = models.IntegerField('性別', choices=GENDER_CHOICE, default=NOT_CHOICE)
    phone = models.CharField('TEL', max_length=20, null=True, blank=True)
    cell_phone = models.CharField('携帯', max_length=20, null=True, blank=True)
    postal_code = models.CharField('郵便番号', max_length=20, null=True, blank=True)
    address1 = models.CharField('住所1', max_length=255, null=True, blank=True)
    address2 = models.CharField('住所2', max_length=255, null=True, blank=True)
    mail = models.EmailField('MAIL', null=True, blank=True)
    job = models.CharField('職業', max_length=100, null=True, blank=True)
    company = models.CharField('勤め先', max_length=100, null=True, blank=True)
    company_department = models.CharField('所属部署', max_length=100, null=True, blank=True)
    company_roll = models.CharField('役職', max_length=100, null=True, blank=True)
    company_address = models.CharField('勤務先住所', max_length=255, null=True, blank=True)
    company_tel = models.CharField('勤務先連絡先', max_length=20, null=True, blank=True)
    company_url = models.URLField('勤め先URL', max_length=256, null=True, blank=True)

    registration_date = models.DateField('ゲスト登録日', default=date.today)

    # gift information
    gift = models.ManyToManyField('Gift', related_name='guests', blank=True, verbose_name='ギフト情報')

    # contract information
    customer_of = models.ForeignKey(Mediator, on_delete=models.SET_NULL, null=True)
    related_guest = models.ManyToManyField('Guest', blank=True)
    access_token = models.CharField(max_length=255, unique=True, blank=True)

    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='guest_created_by')
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='guest_updated_by')

    class Meta:
        permissions = (
            ('view_customerguest', 'View Guest who were invited Mediator'),
            # ('view_guest', 'View Guest Detail View'),
        )

    @property
    def latest_first_visithistory_set(self):
        return self.visithistory_set.order_by('-id')

    def get_absolute_url(self):
        return reverse('ceylontime:guest-detail', args=[self.pk])

    def __str__(self):
        return f'{self.last_name} {self.first_name}'


class GuestFamily(models.Model):

    guest = models.ForeignKey('Guest', on_delete=models.CASCADE, related_name='family', verbose_name='主会員')
    name = models.CharField('氏名', max_length=30)
    birthday = models.DateField('生年月日', null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='guest_family_created_by')
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='guest_family_updated_by')


class VisitHistory(models.Model):

    # visit status
    NOT_VISIT = 0
    IN_VISIT = 1

    VISIT_STATUS = [
        (NOT_VISIT, '--'),
        (IN_VISIT, '来店中'),
    ]
    # service_pattern
    ALL_SERVICE = 0
    AWATOYO = 1
    NON_SERVICE = 2
    SERVICE_PATTERN = (
        (ALL_SERVICE, '全てサービス'),
        (AWATOYO, 'AWATOYO'),
        (NON_SERVICE, 'サービス無し'),
    )

    visit_status = models.IntegerField('来店状況', default=0, choices=VISIT_STATUS)
    guest_name = models.ForeignKey(Guest, on_delete=models.CASCADE)
    in_shop = models.ForeignKey(Shop, default=3, on_delete=models.CASCADE, verbose_name='来店店舗')
    visit_date = models.DateTimeField('来店日', auto_now_add=True)
    number_of_visitors = models.IntegerField('来店人数')
    payment = models.IntegerField('売上金額', default=0, blank=True, null=True)
    service_pattern = models.IntegerField('サービス区別', choices=SERVICE_PATTERN, default=NON_SERVICE)
    exist_reservation = models.BooleanField('予約有無', default=False)
    reservation_remark = models.CharField('予約情報備考', max_length=256, null=True, blank=True)
    salesperson = models.CharField('担当（ばんせい）', max_length=32, null=True, blank=True)
    comment = models.TextField('コメント', blank=True, default='')
    register_date = models.DateField('更新日時', auto_now=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='visit_history_created_by')
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='visit_history_updated_by')

    def get_absolute_url(self):
        return reverse('ceylontime:visit-detail', args=[self.pk])

    def __str__(self):
        return '{} {}: {}, {}'.format(self.guest_name.last_name, self.guest_name.first_name, self.visit_date, self.payment)

MAX_SUBJECT_LENGTH = 1000
MAX_BODY_LENGTH = 20000

class DMTemplate(models.Model):
    name = models.CharField(
        max_length=100,
        verbose_name='テンプレート名',
        unique=True
    )
    subject = models.CharField(
        max_length=MAX_SUBJECT_LENGTH,
        verbose_name='件名'
    )
    body = models.TextField(
        max_length=MAX_BODY_LENGTH,
        verbose_name='本文'
    )
    test_mail = models.EmailField(
        'メールアドレス',
        null=True,
        blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='dm_template_created_by')
    updated_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='dm_template_updated_by')

    def __str__(self):
        return self.subject

    def get_absolute_url(self):
        return reverse('ceylontime:dm-template-list')

class DMHistory(models.Model):
    sender = models.EmailField()
    receivers = models.ManyToManyField(Guest)
    subject = models.CharField(max_length=MAX_SUBJECT_LENGTH)
    body = models.TextField(max_length=MAX_BODY_LENGTH)
    datetime = models.DateTimeField(auto_now_add=True)
    sent_by = models.ForeignKey(User, null=True, on_delete=models.CASCADE, related_name='dms')

    def __str__(self):
        return '{}: {}'.format(self.subject, self.sent_by)


class Gift(models.Model):

    # gift type
    DEFAULT = 0
    YEAR_END_GIFT = 1
    SUMMER_GIFT = 2

    GIFT_TYPE = [
     (DEFAULT, 'ギフト'),
     (YEAR_END_GIFT, 'お歳暮'),
     (SUMMER_GIFT, 'お中元'),
    ]

    gift_type = models.IntegerField(choices=GIFT_TYPE, default=DEFAULT, verbose_name='ギフト種類')
    send_bool = models.BooleanField(default=False, verbose_name='送付状況')
    gift = models.CharField(max_length=50, blank=True, null=True, verbose_name='ギフト名')
    send_date = models.DateField(verbose_name='送付年月日', blank=True, null=True)

    def __str__(self):
        return self.gift
