from django import forms
from .models import DMTemplate

from django_summernote.widgets import SummernoteWidget


class DMTemplateCreateForm(forms.ModelForm):
    class Meta:
        model = DMTemplate
        fields = ['name', 'subject', 'body', 'test_mail']
        widgets = {
                'body': SummernoteWidget(attrs={'summernote': {'width': '100%'}}),
        }
