# Generated by Django 2.1.4 on 2019-06-28 06:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ceylontime', '0007_dmtemplate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dmtemplate',
            name='name',
            field=models.CharField(max_length=100, unique=True, verbose_name='テンプレート名'),
        ),
    ]
