# Generated by Django 2.1.4 on 2021-10-12 00:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ceylontime', '0013_auto_20200205_1117'),
    ]

    operations = [
        migrations.AddField(
            model_name='guest',
            name='company_url',
            field=models.URLField(blank=True, max_length=256, null=True, verbose_name='勤め先URL'),
        ),
        migrations.AlterField(
            model_name='dmtemplate',
            name='test_mail',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='メールアドレス'),
        ),
    ]
