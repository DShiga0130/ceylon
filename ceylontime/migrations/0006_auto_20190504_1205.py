# Generated by Django 2.1.4 on 2019-05-04 03:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ceylontime', '0005_auto_20190503_1108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dmhistory',
            name='body',
            field=models.TextField(max_length=20000),
        ),
        migrations.AlterField(
            model_name='dmhistory',
            name='subject',
            field=models.CharField(max_length=1000),
        ),
    ]
