from django.contrib import admin


from .models import Mediator, Guest, VisitHistory, Shop, Gift

# Register your models here.
admin.site.register(Mediator)
admin.site.register(Guest)
admin.site.register(VisitHistory)
admin.site.register(Shop)
admin.site.register(Gift)
