$(function() {
	$.fn.dynamicSearchBox = function(searchUrl, placeholder) {
		return this.each(function() {
			var $self = $(this)
				.data('current_id', null)
				.data('last_keyword', '')
				.data('search_url', searchUrl)
				.addClass('dynamic-search-box')
				.html(
					'<input type="text" />' +
					'<div class="loader">' +
						'<div class="sk-fading-circle">' +
							'<div class="sk-circle1 sk-circle"></div>' +
							'<div class="sk-circle2 sk-circle"></div>' +
							'<div class="sk-circle3 sk-circle"></div>' +
							'<div class="sk-circle4 sk-circle"></div>' +
							'<div class="sk-circle5 sk-circle"></div>' +
							'<div class="sk-circle6 sk-circle"></div>' +
							'<div class="sk-circle7 sk-circle"></div>' +
							'<div class="sk-circle8 sk-circle"></div>' +
							'<div class="sk-circle9 sk-circle"></div>' +
							'<div class="sk-circle10 sk-circle"></div>' +
							'<div class="sk-circle11 sk-circle"></div>' +
							'<div class="sk-circle12 sk-circle"></div>' +
						'</div>' +
					'</div>' +
					'<ul class="candidates thin-scrollbar">' +
					'</ul>' +
					'<span class="current"><span class="value"></span><span class="delete"></span></span>'
				);
			if (placeholder) $('input', $self).attr('placeholder', placeholder);
			$self
				.on('focus', 'input', function(e) {
					if ($('.candidates .candidate', $self).length > 0 && $(this).val().length > 0) {
						$('.candidates', $self).show().scrollTop(0);
						$('.candidates .candidate', $self).removeClass('selected').first().addClass('selected');
					} else {
						$self._searchCandidates($(this).val());
					}
				})
				.on('keyup', 'input', function(e) {
					var $selected = $('.candidates .candidate.selected', $self);
					if (e.keyCode == 13) {
						if ($selected.length == 0) {
							return;
						}
						$self._setCurrent($selected);
						$(this).blur();
					} else if (e.keyCode == 38) {	// up
						e.preventDefault();
						$selected.removeClass('selected');
						if ($selected.length == 0) {
							$('.candidates .candidate', $self).first().addClass('selected');
							return;
						}
						var $prev = $selected.prev('.candidate');
						if ($prev.length == 0) {
							$selected.addClass('selected');
							return;
						} else {
							$prev.addClass('selected');
							var $container = $('.candidates', $self);
							if ($prev.position().top < 0) {
								$container.scrollTop(0);
								var top = $prev.position().top;
								$container.scrollTop(top);
							}
						}
					} else if (e.keyCode == 40) {	// down
						e.preventDefault();
						var $selected = $('.candidates .candidate.selected', $self);
						$selected.removeClass('selected');
						if ($selected.length == 0) {
							$('.candidates li', $self).first().addClass('selected');
							return;
						}
						var $next = $selected.next('.candidate');
						if ($next.length == 0) {
							$selected.addClass('selected');
							return;
						} else {
							$next.addClass('selected');
							var $container = $('.candidates', $self);
							var bottom = $next.position().top + $next.outerHeight(true) + $container.scrollTop();
							$container.scrollTop(bottom - $container.height());
						}
					} else {
						var interval = $self.data('interval') || null;
						if (interval) window.clearTimeout(interval);
						interval = setTimeout(function() {
							$self._searchCandidates($('input', $self).val());
						}, 200);
						$self.data('interval', interval);
					}
				})
				.on('click', '.candidate', function(e) {
					e.preventDefault();
					$self._setCurrent($(this));
				});
		}); // end of each
	};	// end of dynamicSearch
	$.fn.extend({
		_setCurrent: function($item) {
			if (!$(this).hasClass('dynamic-search-box')) return;

			var $self = $(this);
			var href = $('a', $item).attr('href');
			window.location.href = href;
		},
		_searchCandidates: function(keyword) {
			if (!$(this).hasClass('dynamic-search-box')) return [];

			if (keyword.length == 0) {
				$('.candidates', $self).hide();
				return;
			}
			var $self = $(this);
			var lastKeyword = $self.data('last_keyword');
			if (keyword == lastKeyword) return;
			var searchUrl = $self.data('search_url');
			var date = new Date() ;
			var timestamp = date.getTime();
			$('.loader', $self).show();
			$.ajax({
				url: searchUrl,
				type: 'GET',
				data: { timestamp: timestamp, keyword: keyword },
				dataType: 'json',
				success: function(data) {
					var latest = $self.data('timestamp') || 0;
					if (parseInt(data['timestamp']) > latest) {
						var $container = $('.candidates', $self);
						$container.empty();
						var items = data['items'] || '';
						if (items.length == 0) {
							$container.append('<li>該当なし</li>');
							$container.show();
						} else {
							$container.append(items);
							$('.candidate', $container).first().addClass('selected');
							$container.show();
							$('li', $container).addClass('candidate');
							$('*', $container)
								.css('cursor', 'default')
								.on('click', function(e) { e.preventDefault(); });
						}
						$('.candidate', $container).each(function() {
							$self._ellipsis($(this));
						});
						$self.data('timestamp', parseInt(data['timestamp']));
						$self.data('last_keyword', keyword);
					}
					$('.loader', $self).hide();
				},
				error: function(response) {
					$('.loader', $self).hide();
				}
			});
		},
		_ellipsis: function($target) {
			if (!$(this).hasClass('dynamic-search-box')) return;

			return this.each(function() {

				if ($target.html() == '') return;
				var $tmp = $target.clone()
												.hide()
												.width('auto')
												.css({ 'overflow': 'visible' });
				$target.after($tmp);
				var text = $target.html();
				var width = $target.width();
				while (text.length > 0 && $tmp.width() > width) {
					text = text.substr(0, text.length - 1);
					$tmp.html(text + '...');
				}
				$target.html($tmp.html());
				$tmp.remove();
			});
		},
	});	// end of extend
});
